var searchData=
[
  ['valid_5ffilename',['valid_filename',['../mapping-4.html#a8034c4930d949c390f8bc2c0852028c2',1,'Mapping::mapping-4']]],
  ['verbosity',['verbosity',['../classMapping_1_1mapper_1_1Mapper.html#a7a4c2debb00734eefbd1dbbd880375db',1,'Mapping.mapper.Mapper.verbosity()'],['../classMapping_1_1mapper_1_1Bowtie2.html#a71cdfb027466d62c8963a8bfbf060648',1,'Mapping.mapper.Bowtie2.verbosity()'],['../classMapping_1_1mapper_1_1BWAMem.html#ae35845fb43e3dadf02d2be98408afcb3',1,'Mapping.mapper.BWAMem.verbosity()']]],
  ['version',['version',['../classMapping_1_1mapping-4_1_1MAPPING.html#a1ebecb0b4fec392d60654a4b58041b8b',1,'Mapping.mapping-4.MAPPING.version()'],['../classMapping_1_1mapper_1_1Mapper.html#a8ccb41a394b8749fc6f54790bb66db09',1,'Mapping.mapper.Mapper.version()'],['../classMapping_1_1mapper_1_1Bowtie2.html#a5d99744f969f9e4033bdddb3568f0aa4',1,'Mapping.mapper.Bowtie2.version()'],['../classMapping_1_1mapper_1_1BWAMem.html#afb30d7ce0786288c4d715c48410526ae',1,'Mapping.mapper.BWAMem.version()']]]
];
