var searchData=
[
  ['query',['query',['../classMapping_1_1mapper_1_1Mapper.html#a06310bda9b6ba947f7fdc93517dfd530',1,'Mapping.mapper.Mapper.query()'],['../classMapping_1_1mapper_1_1Bowtie2.html#ae73ee72d731c4db1564b378ff7cb0dc4',1,'Mapping.mapper.Bowtie2.query()'],['../classMapping_1_1mapper_1_1BWAMem.html#abe728b3df7255c8a9eb0e7bec4352bdf',1,'Mapping.mapper.BWAMem.query()']]],
  ['query_5ffasta',['query_fasta',['../classMapping_1_1mapper_1_1Bowtie2.html#ad14d3c1a536f6e8479cfbd52026bb7ee',1,'Mapping.mapper.Bowtie2.query_fasta()'],['../classMapping_1_1mapper_1_1BWAMem.html#aa1d5a28f5dbe33c8229b226005578115',1,'Mapping.mapper.BWAMem.query_fasta()']]],
  ['query_5fpaired',['query_paired',['../classMapping_1_1mapper_1_1Mapper.html#a815750b8ca5bc1943884bca47525a776',1,'Mapping.mapper.Mapper.query_paired()'],['../classMapping_1_1mapper_1_1Bowtie2.html#a1d20fee85774eee56d5a6570e4adc435',1,'Mapping.mapper.Bowtie2.query_paired()'],['../classMapping_1_1mapper_1_1BWAMem.html#a64a5aab0158a19ef9c74907c8696c2b1',1,'Mapping.mapper.BWAMem.query_paired()']]]
];
