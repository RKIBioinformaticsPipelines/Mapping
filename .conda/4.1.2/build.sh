#!/bin/bash

set -eu -o pipefail
# grep adapters from trimmomatic

DATADIR=$PREFIX/opt/mapping-4

mkdir -p $DATADIR

cp -r * $DATADIR/.
ln -s $DATADIR/mapping-4 $PREFIX/bin/.

chmod +rwx $PREFIX/bin/mapping-4 

